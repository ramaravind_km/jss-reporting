import argparse

from report_maker import ReportMaker

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--ignoreTestCases', type=int, default=0, help="Provide 1 to ignore test cases, else provide 0 to include them. Defaults to 0.")
    parser.add_argument('--reportDate', type=str, default='', help="Provide a date in YYYY/MM/DD format to get reports for that date. By default, data of all dates are considered.")
    parser.add_argument('--connectToProdDB', type=int, default=0, help="Provide 1 to connect to Nikshay-production DB, else provide 0 to connect to Nikshay-reporting DB. Defaults to 0.")
    args = parser.parse_args()

    testStr = ' with test cases' if args.ignoreTestCases == 0 else ' without test cases'
    reportStr = ' for ' + args.reportDate if args.reportDate != '' else ''
    connStr = ' from Nikshay-production DB' if args.connectToProdDB == 1 else ' from Nikshay-reporting DB'
    print('Creating JSS reports' + testStr + reportStr + connStr + '...')

    reportMaker = ReportMaker(args.ignoreTestCases, args.reportDate, args.connectToProdDB)
    reportMaker.create_reports()
    print('Reports created.')
