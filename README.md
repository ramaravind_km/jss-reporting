# JSS reporting

Create Daily Reporting mechanism for JSS. Reference issue: https://gitlab.com/everwell/product-work/-/issues/1080

## How to run
Download this repo and run the following python script from the top-most folder of the repo:

```
python get_forms_report.py [-h] [--ignoreTestCases IGNORETESTCASES] [--reportDate REPORTDATE] [--connectToProdDB CONNECTTOPRODDB]

optional arguments:
  -h, --help            show this help message and exit
  --ignoreTestCases IGNORETESTCASES
                        Provide 1 to ignore test cases, else provide 0 to include them. Defaults to 0.
  --reportDate REPORTDATE
                        Provide a date in YYYY/MM/DD format to get reports for that date. By default, data of all dates are considered.
  --connectToProdDB CONNECTTOPRODDB
                        Provide 1 to connect to Nikshay-production DB, else provide 0 to connect to Nikshay-reporting DB. Defaults to 0.
```

## Requirements                
The following packages are required in Python:

* numpy
* pandas
* pyodbc
* pymongo

In order to connect to the *forms MongoDB*, your public IP needs to be whitelisted. If you have access, you should add your IP to the following:

```
VM : apps-server-beta-02
rule : fomio-db
```

## Outcome
A folder named `Results/` is created at the top-most folder upon successful run. Separate files are created for each state inside the `Results/` directory. File format is `JSS_reports<_reportDate>` if `reportDate` is provided, else it is `JSS_reports`. 
