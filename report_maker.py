import os
import numpy as np
import pandas as pd

import pyodbc # pip install pyodbc
from pymongo import MongoClient # pip install pymongo
import copy

class ReportMaker:
    def __init__(self, ignoreTestCases, reportDate, connectToProdDB):

        self.ignoreTestCases = ignoreTestCases
        self.reportDate = reportDate

        # set up pyodbc
        if connectToProdDB:
            self.conn = pyodbc.connect("Driver={SQL Server}; server=nikprod.centralindia.cloudapp.azure.com,3314; database=nikshay-production; uid=India-TB; pwd=gHqPDjac5WxPTZV8")
        else:
            self.conn = pyodbc.connect("Driver={SQL Server}; server=nikshay-report.centralindia.cloudapp.azure.com,3341; database=Nikshay-Reporting; uid=Siddharth;pwd=dS5eK3bU4zA5xQ4n")

        # set up pymongo
        CONNECTION_STRING = "mongodb://root:VqsEqqTe8HEeGGNL@52.172.139.56:27017"
        client = MongoClient(CONNECTION_STRING)
        db = client['formio']
        self.forms_collection = db["forms"]
        self.submissions_collection = db['submissions']

    def create_reports(self):
        hierarchy_map = self.get_hierarhcy_map()
        forms_name_title_map = self.get_forms_map()
        all_forms_dict = self.get_forms_data(forms_name_title_map)
        fvc_summary = self.get_fvc_summary(hierarchy_map, forms_name_title_map, all_forms_dict)
        patient_interview_summary = self.get_patient_interview_summary( hierarchy_map, forms_name_title_map, all_forms_dict['Patient Interview'])
        self.consolidate_data(hierarchy_map, all_forms_dict, fvc_summary, patient_interview_summary)

    def get_hierarhcy_map(self):
        filtered_hierarchy = pd.read_excel('JSS_Districts.xlsx') # reports required for only this state-district pair
        filtered_hids = filtered_hierarchy['HierarchyID'].tolist()
        placeholders = ", ".join(["?"] * len(filtered_hids))

        # get hierarchy ids, state, district from server
        cursor = self.conn.cursor()
        sql_query = "select Id as HierarchyId, Level_2_Name as State, Name as District from _Hierarchy where Level=3 and Name!='Non-Functional' and Id in ("+placeholders+")"
        executor = cursor.execute(sql_query, filtered_hids)
        results = cursor.fetchall()
        hierarchy_map = pd.DataFrame.from_records(results, columns=['HierarchyId', 'State', 'District'])

        # removing duplicates and dummy cases
        hierarchy_map['State'] = hierarchy_map['State'].replace(['ARUNACHAL PRADESH'], 'Arunachal Pradesh')
        hierarchy_map['State'] = hierarchy_map['State'].replace(['CHHATTISGARH'], 'Arunachal Pradesh')
        hierarchy_map = hierarchy_map[hierarchy_map['State'] != 'DemoState'].reset_index()
        hierarchy_map = hierarchy_map[~hierarchy_map['District'].isin(['.', 'Kolkata (NON FUNCTIONAL DISTRICT)', 'Demo District'])]

        return hierarchy_map

    def get_forms_map(self):
        # get form names and mapping between frontend and backend display values
        forms_name_title_map = []
        forms = self.forms_collection.find() # pymongo cursor
        for form in forms:
            forms_name_title_map.append([form['name'], form['title']]) # name is backend variable and title is frontend display value
        forms_name_title_map = pd.DataFrame(forms_name_title_map, columns=['name', 'title'])
        return forms_name_title_map

    def addCheckBoxValues(self, data, form, one_form):
        checkBoxes = self.getCheckBoxes(form)
        for field, options in checkBoxes.items():
            if field in data:
                for option in options:
                    shortOptionValue = option['value']
                    originalOptionValue = option['label'] # label: frontent, key: backend
                    if shortOptionValue in data[field]:
                        data[field][originalOptionValue] = data[field][shortOptionValue]
                        del data[field][shortOptionValue]
        return data

    def getCheckBoxes(self, form, checkBoxes = {}):
        if 'components' in form:
            for component in form['components']:
                if 'type' in component and 'key' in component and 'values' in component and component['type'] == "selectboxes":
                    checkBoxes[component['key']] = component['values']
                if 'components' in component:
                    self.getCheckBoxes(component, checkBoxes)
        return checkBoxes

    def getAllColumns(self, form, keyLabel={}):
        if 'components' in form:
            for component in form['components']:
                if component['input'] and component['type']!='button':
                    keyLabel[component['key']] = component['label']
                if 'components' in component:
                    self.getAllColumns(component, keyLabel)
        return keyLabel

    def get_forms_data(self, forms_name_title_map):
        all_forms_dict = {}
        all_fvc_form_names = [one_form for one_form in forms_name_title_map['name'].tolist() if one_form.startswith('fvc')]

        for one_form in all_fvc_form_names + ['patientInterview']:
            form = self.forms_collection.find_one({'name': one_form})
            form_id = form['_id']
            form_title = form['title']

            # get all fields and mapping between frontend and backend
            colsBackendFrontend = {}
            colsBackendFrontend = self.getAllColumns(form, colsBackendFrontend)

            # get all form submissions
            submissions = self.submissions_collection.find({'form': form_id})
            all_submissions = []
            for submission in submissions:
                all_submissions.append(copy.deepcopy(submission))

            # get summary stats
            rows = []
            for submission in all_submissions:

                # get data for required report day
                if self.reportDate != '':
                    created_at = submission['created']
                    if created_at < pd.Timestamp(self.reportDate) and created_at > (pd.Timestamp(self.reportDate) + pd.DateOffset(hours=23, minutes=59, seconds=59)):
                        continue

                # filter out deleted submission
                if submission['deleted'] is not None:
                    continue

                # get submission data
                data = self.addCheckBoxValues(submission['data'], form, one_form)

                # filter out test data
                if self.ignoreTestCases == 1:
                    if 'name' in data and 'test' in data['name'].lower():
                        continue
                    if 'patientName' in data and 'test' in data['patientName'].lower():
                        continue

                # store entries
                row = []
                row.append(data['hierarchyId'])
                row.append(data['selectedDate'])
                for col in colsBackendFrontend.keys():
                    if col not in data:
                        row.append(None)
                        continue

                    if isinstance(data[col], dict):
                        try:
                            temp = sum(data[col].values)
                        except:
                            row.append(data[col])

                        colval = ''
                        for subkey in data[col]:
                            if data[col][subkey] == True:
                                colval = colval + ', '
                        colval = colval[:-2]
                    else:
                        row.append(data[col])
                row.append(submission['created'])
                row.append(submission['modified'])
                rows.append(row)

            cols = ['Hierarchy ID', 'Selected Date'] + list(colsBackendFrontend.values()) + ['Submitted', 'Updated']
            form_df = pd.DataFrame(rows, columns=cols)
            all_forms_dict[form_title] = form_df

        return all_forms_dict

    def get_fvc_summary(self, hierarchy_map, forms_name_title_map, all_forms_dict):
        fvc_summary_df = pd.DataFrame()
        fvc_summary_df['State'] = np.array(hierarchy_map['State'].tolist())
        fvc_summary_df['District'] = np.array(hierarchy_map['District'].tolist())

        for one_form in forms_name_title_map['title'].tolist():
            if not one_form.startswith('FVC'):
                continue

            one_form_df = all_forms_dict[one_form]
            fvc_summary_col = []

            for hid in hierarchy_map['HierarchyId'].tolist():
                val = one_form_df[one_form_df['Hierarchy ID']==str(hid)].shape[0]
                fvc_summary_col.append(val)

            fvc_summary_df[one_form] = np.array(fvc_summary_col)

        return fvc_summary_df

    def get_patient_interview_summary(self, hierarchy_map, forms_name_title_map, patientInterviewDF):
        patient_interview_ids = patientInterviewDF['Patient ID (Nikshay ID)'].tolist()
        if len(patient_interview_ids) == 0:
            interview_summary_df = pd.DataFrame(0, index=np.arange(hierarchy_map.shape[0]), columns=['State', 'District', 'DSTB Patients', 'DRTB Patients', 'TB Comorbidity', 'Paediatric TB'])
            interview_summary_df['State'] = np.array(hierarchy_map['State'].tolist())
            interview_summary_df['District'] = np.array(hierarchy_map['District'].tolist())
            return interview_summary_df

        placeholders = ", ".join(["?"] * len(patient_interview_ids))

        cursor = self.conn.cursor()

        # get info from patient table
        sql_query = "select Id, TypeOfCase, Age, HIVTestStatus from _Patient where Id in ("+placeholders+")"
        executor = cursor.execute(sql_query, patient_interview_ids)
        results = cursor.fetchall()
        from_patient_table = pd.DataFrame.from_records(results, columns=['PatientId', 'TypeOfCase', 'Age', 'HIVTestStatus'])

        # get info from comorbidity table
        cursor = self.conn.cursor()
        sql_query = "select PatientId from _Comorbidity where DiabetesStatus='Diabetic' and PatientId in ("+placeholders+")"
        executor = cursor.execute(sql_query, patient_interview_ids)
        results = cursor.fetchall()
        from_comorbidity_table = pd.DataFrame.from_records(results, columns=['PatientId'])

        interview_summary_df = pd.DataFrame()
        interview_summary_df['State'] = np.array(hierarchy_map['State'].tolist())
        interview_summary_df['District'] = np.array(hierarchy_map['District'].tolist())

        dstb = []
        drtb = []
        comorbid = []
        pediatric = []
        for hid in hierarchy_map['HierarchyId'].tolist():
            tempdf = patientInterviewDF[patientInterviewDF['Hierarchy ID']==str(hid)]
            pids = tempdf['Patient ID (Nikshay ID)'].tolist()

            if len(pids) == 0:
                dstb.append(0)
                drtb.append(0)
                comorbid.append(0)
                pediatric.append(0)
                continue

            subdf = from_patient_table[from_patient_table['PatientId'].isin(pids)]
            not_present = set(pids).difference(from_patient_table['PatientId'].tolist())
            is_null = subdf['TypeOfCase'].isnull().sum()
            val_dict = subdf['TypeOfCase'].value_counts().to_dict()

            if 'PMDT' in val_dict:
                drtb.append(val_dict['PMDT'])
                dstb.append(sum(val_dict.values()) - val_dict['PMDT'] + is_null)
            else:
                drtb.append(0)
                dstb.append(subdf.shape[0] + len(not_present))

            pediatric.append(subdf[(subdf['Age']>0) & (subdf['Age']<=14)].shape[0])

            val_dict = subdf['HIVTestStatus'].value_counts().to_dict()
            val = 0
            if 'Positive' in val_dict:
                val += val_dict['Positive']
            if 'Reactive' in val_dict:
                val += val_dict['Reactive']
            val += from_comorbidity_table[from_comorbidity_table['PatientId'].isin(pids)].shape[0]
            comorbid.append(val)

        interview_summary_df['DSTB Patients'] = np.array(dstb)
        interview_summary_df['DRTB Patients'] = np.array(drtb)
        interview_summary_df['TB Comorbidity'] = np.array(comorbid)
        interview_summary_df['Paediatric TB'] = np.array(pediatric)

        return interview_summary_df

    def consolidate_data(self, hierarchy_map, all_forms_dict, fvc_summary, patient_interview_summary):
        directory = os.path.join('Results', 'All India')
        if not os.path.exists(directory):
            os.makedirs(directory)
        self.store_data(directory, 'All India', all_forms_dict, fvc_summary, patient_interview_summary)

        for state, _ in hierarchy_map.groupby('State'):
            directory = os.path.join('Results', state)
            if not os.path.exists(directory):
                os.makedirs(directory)
            hids = hierarchy_map[hierarchy_map['State']==state]['HierarchyId'].tolist()
            hids = [str(this_id) for this_id in hids]
            self.store_data(directory, state, all_forms_dict, fvc_summary, patient_interview_summary, hids)


    def store_data(self, directory, state, all_forms_dict, fvc_summary, patient_interview_summary, hids=None):
        filename = 'JSS_reports_' + self.reportDate if self.reportDate != '' else 'JSS_reports'
        filename = filename.replace('/', '-')
        filename = os.path.join(directory, filename)
        writer = pd.ExcelWriter(filename+'.xlsx')

        if state == 'All India':
            fvc_summary.to_excel(writer, 'Field Visit - Form Fill Update', index=False)
            patient_interview_summary.to_excel(writer, 'Patient Info - Form FIll Update', index=False)
        else:
            summary_df = fvc_summary[fvc_summary['State']==state]
            summary_df.to_excel(writer, 'Field Visit - Form Fill Update', index=False)
            summary_df = patient_interview_summary[patient_interview_summary['State']==state]
            summary_df.to_excel(writer, 'Patient Info - Form FIll Update', index=False)

        for one_form in all_forms_dict:
            one_form_name = one_form.replace('/', '-')
            if len(one_form_name) > 31:  # excel sheet names with len>31 creates weird issues (https://github.com/PHPOffice/PhpSpreadsheet/pull/177)
                one_form_name = one_form_name[:29]+'..'

            if state == "All India":
                all_forms_dict[one_form].to_excel(writer, one_form_name, index=False)
            else:
                all_forms_dict[one_form][all_forms_dict[one_form]['Hierarchy ID'].isin(hids)].to_excel(writer, one_form_name, index=False)

        writer.save()
        writer.close()
